<?php

namespace view;

class View {

    private static $recipeUnique = "recipe"
            , $login = "log_in"
            , $username = "username"
            , $password = "password";

    /**
     * Returns a string with the current time and date specially formatted
     * @return string
     */
    private function getTimeAndDate() {
        $weekDayArray = array("Måndag", "Tisdag", "Onsdag", "Torsdag", "Fredag", "Lördag", "Söndag");
        $monthArray = array("Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December");
        $toReturn = $weekDayArray[date("N") - 1] . ", den " . date("j") . " " . $monthArray[date("n") - 1] . " år ";
        $toReturn .= date("Y") . ". Klockan är [" . date("H:i:s") . "]";
        return $toReturn;
    }

    /**
     * Show page where you log in
     */
    public function showLoginPage() {
        $this->showPage("Logga in", '<form method="post">
    <div>
        <label for="username">Username:</label>
        <input type="text" id="' . self::$username . '" name="' . self::$username . '" value="" />
    </div>
    <div>
        <label for="password">Password:</label>
        <input type="password" id="' . self::$password . '" name="' . self::$password . '" />
    </div>
    <label for="remember">Remember me:</label>
    <input type="checkbox" name="remember" id="remember" value="1" />
    <input type="hidden" name="' . self::$login . '" />
    <input type="submit" value="Log in" />
</form>');
    }

    /**
     * Page to show when logged in
     */
    public function showLoggedInPage() {
        $this->showPage("Logged in", '<div>
            <a href="?logout">Log out</a>
        </div>');
    }

    /**
     * Shows a list of the recipes
     * @param \model\RecipeList $recipes
     */
    public function showRecipesOverview(array $recipes) {
        $body = "<a href='?" . self::$login . "'>Logga in</a>";
        foreach ($recipes as $recipe) {
            $body .= $this->getRecipeForOverview($recipe);
        }
        $this->showPage("Recipes", $body);
    }

    /**
     * @param \model\recipe\Recipe $recipe
     * @return string
     */
    private function getRecipeForOverview(\model\recipe\Recipe $recipe) {
        $toReturn = "<article class='recipeOverview'>"
                . "<a href='?recipe=" . $recipe->getUnique() . "'>"
                . "<img src='' />"
                . "<h3>" . $recipe->getName() . "</h3>"
                . "</article>";
        return $toReturn;
    }

    /**
     * Checks if you are trying to log in.
     * @return boolean
     */
    public function tryingToShowLogInPage() {
        return (filter_input(INPUT_GET, self::$login) !== null);
    }

    /**
     * Checks if you are trying to log in from the form.
     * @return type
     */
    public function tryingToLogIn() {
        return (filter_input(INPUT_POST, self::$login) !== null);
    }

    /**
     * Checks if you are trying to see a recipe.
     * @return boolean
     */
    public function tryingToSeeRecipe() {
        return (filter_input(INPUT_GET, self::$recipeUnique) !== null);
    }

    /**
     * NOTE! Only use this after tryingToSeeRecipe returnes true!
     * Gets the unique identifier for the recipe
     * @return int
     */
    public function getRecipeUnique() {
        return filter_input(INPUT_GET, self::$recipeUnique);
    }

    /**
     * Returns user when session contains username and password,
     * returns an empty user when session doesn't contain username and password
     * @return \model\user\User
     */
    public function getUserFromSession() {
        $username = isset($_SESSION[self::$username]) ?
                $_SESSION[self::$username] : '';
        $password = isset($_SESSION[self::$password]) ?
                $_SESSION[self::$password] : '';
        return new \model\user\User($username, $password);
    }

    /**
     * Redirects the page to index
     */
    public function redirectToIndex() {
        header("Location: index.php");
    }

    /**
     * Shows the recipe page
     * @param \model\recipe\Recipe $recipe
     */
    public function showRecipe(\model\recipe\Recipe $recipe) {
        $body = $this->backToIndex() . "<article>"
                . "<h3>" . $recipe->getName() . "</h3>"
                . "<p>" . $recipe->getPersons() . " personer</p>"
                . "<ul>";
        foreach ($recipe->getIngredients() as $ingredient) {
            $body .= "<li>" . $ingredient->getAmount() . " "
                    . $ingredient->getUnit() . " "
                    . $ingredient->getName() . "</li>";
        }
        $body .= "</ul><h4>Såhär gör man</h4>"
                . "<div>"
                . $this->paragraphNewLines($recipe->getDirections())
                . "</div>"
                . "</article>";
        $this->showPage("Recipes", $body);
    }

    /**
     * Splits new lines in text and returns them surrounded with paragraph
     * tags <p>
     * @param string $text
     * @return string
     */
    private function paragraphNewLines($text) {
        $arr = explode("\n", $text);
        $out = '';

        for ($i = 0; $i < count($arr); $i++) {
            if (strlen(trim($arr[$i])) > 0) {
                $out.='<p>' . trim($arr[$i]) . '</p>';
            }
        }
        return $out;
    }

    private function backToIndex() {
        return "<a href='index.php'>Back to index</a>";
    }

    /**
     * Displays page on screen.
     * @param String $title Title of page
     * @param String $body Page body
     */
    private function showPage($title, $body) {
        echo '<!doctype html>
                <html lang="sv">
                <head>
                     <meta charset="utf-8">
                     <title>' . $title . '</title>
                     <meta name="description" content="The HTML5 Herald">
                     <meta name="author" content="SitePoint">
                     <link rel="stylesheet" href="css/style.css">
                 </head>
                 <body>
                 ' . $body . '
                 </body>
              </html>';
    }

}
