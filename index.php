<?php
require 'kint/Kint.class.php';
//Kint::dump($recipes);

session_start();
require_once './view/View.php';
require_once './controller/Controller.php';

/*require_once './model/User.php';
$temp = new model\User("Testar", "lite");
$temp1 = new model\User("Testar", "litee");
$toPrint = $temp->compare($temp1) ? 'true' : 'false';*/

$page = new \view\View();
$controller = new controller\Controller($page);
$controller->runApplication();