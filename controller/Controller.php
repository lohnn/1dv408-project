<?php

namespace controller;

require_once 'model/recipe/RecipeManager.php';
require_once 'model/recipe/RecipeList.php';
require_once 'model/user/UserManager.php';

class Controller {

    /**
     * @var \view\View
     */
    private $view;

    /**
     * @var \model\recipe\RecipeManager
     */
    private $recipeManager;

    /**
     * @var \model\user\UserManager;
     */
    private $userManager;

    public function __construct(\view\View $view) {
        $this->view = $view;
        $this->recipeManager = new \model\recipe\RecipeManager();
        $this->userManager = new \model\user\UserManager();
    }

    public function runApplication() {

        //Has logged in? (session)
        if ($this->userManager->tryLogin($this->view->getUserFromSession())) {
            
        }//Trying to log in (cookie & form)
        if ($this->view->tryingToLogIn()) {
            $this->view->redirectToIndex();
        } else if ($this->view->tryingToShowLogInPage()) {
            $this->view->showLoginPage();
        } else if ($this->view->tryingToSeeRecipe()) {
            $this->view->showRecipe(
                    $this->recipeManager->getRecipe(
                            $this->view->getRecipeUnique()));
        } else {
            $this->view->showRecipesOverview($this->recipeManager->getRecipes());
        }
    }

}
