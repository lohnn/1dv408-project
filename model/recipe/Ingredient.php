<?php

namespace model\recipe;

class Ingredient {

    /**
     * @var float
     */
    private $amount;

    /**
     * @var string
     */
    private $unit;

    /**
     * @var string
     */
    private $name;

    public function __construct($amount, $unit, $name) {
        $this->amount = $amount;
        $this->unit = $unit;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getAmount() {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getUnit() {
        return $this->unit;
    }

}
