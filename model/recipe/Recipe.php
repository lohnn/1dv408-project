<?php

namespace model\recipe;

require_once 'model/recipe/Ingredient.php';

class Recipe {

    /**
     * @var int
     */
    private $unique;

    /**
     * @var string
     */
    private $name;

    /**
     * How many persons are the recipe for
     * @var int
     */
    private $persons;

    /**
     * @var Ingredient
     */
    private $ingredients;

    /**
     * @var string
     */
    private $directions;

    public function __construct($unique, $persons, $name, array $ingredients, $directions) {
        $this->unique = $unique;
        $this->persons = $persons;
        $this->name = $name;
        $this->ingredients = $ingredients;
        $this->directions = $directions;
    }

    /**
     * @return int
     */
    public function getUnique() {
        return $this->unique;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getIngredients() {
        return $this->ingredients;
    }

    /**
     * @return string
     */
    public function getDirections() {
        return $this->directions;
    }

    /**
     * @return int
     */
    public function getPersons() {
        return $this->persons;
    }

}
