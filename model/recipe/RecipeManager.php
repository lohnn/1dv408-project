<?php

namespace model\recipe;

require_once 'model/recipe/Recipe.php';

class RecipeManager {

    /**
     * @var array
     */
    private $recipes = array();

    public function __construct() {
        $this->recipes[0] = new \model\recipe\Recipe(0, 1, "Tacokrydda", array(
            new Ingredient(2, "tsp", "Tacokrydda")
                ), "Blanda hårt");
        $this->recipes[2] = new \model\recipe\Recipe(2, 2, "Tacokrydda med getmjölk", array(
            new Ingredient(2, "tsp", "Tacokrydda"),
            new Ingredient(5, "msk", "Getmjölk")
                ), "Blanda hårt");
        $this->recipes[3] = new \model\recipe\Recipe(3, 4, "Ört- och fetaostboallar i tomatsås"
                , array(
            new Ingredient(400, "g", "Pasta, ex linguini"),
            new Ingredient(1, "burk", "Tomatsås, ca 700g"),
            new Ingredient(1, "", "Ägg"),
            new Ingredient(1, "dl", "Ströbröd"),
            new Ingredient(1, "dl", "Riven gul lök"),
            new Ingredient(1, "dl", "Hackade örter, ex basilika oregano, persilja"),
            new Ingredient(2, "dl", "Mandelmjöl"),
            new Ingredient(2, "dl", "Riven ost"),
            new Ingredient(150, "g", "Smulad fetaost"),
            new Ingredient(3, "msk", "Olivolja")
                ), "Koka pastan enligt anvisningen på förpackningen.\n"
                . "Blanda ihop alla ingredienserna utom oljan till fetaostbollarna. Forma till 24 bollar.\n"
                . "Hetta upp en stekpanna med olja och stek bollarna på låg temeratur ca 8 min.\n"
                . "Värm tomatsåsen och servera med pastan och fetaostbollarna");
    }

    /**
     * @return array
     */
    public function getRecipes() {
        return $this->recipes;
    }

    public function getRecipe($unique) {
        return $this->recipes[$unique];
    }

}
