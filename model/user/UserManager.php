<?php

namespace model\user;

require_once 'UserList.php';

class UserManager {

    /**
     * @var UserList
     */
    private $users;

    /**
     * @var boolean
     */
    private $loggedIn = false;

    private function getUser() {
        
    }

    public function __construct() {
        $this->users = new UserList();
    }

    /**
     * Checks if you are logged in
     * @return boolean True if logged in
     */
    public function isLoggedIn() {
        return $this->loggedIn;
    }

    private function hasUserInfoSession() {
        
    }

    private function hasUserInfoCookie() {
        
    }

    private function getUserInfoSession() {
        
    }

    private function getUserInfoCookie() {
        
    }

    /**
     * Log in as the user, returns true if sucsessful, false if not.
     * @param \model\User $user
     * @return boolean
     */
    public function tryLogin(User $user) {
        foreach ($this->users->getUsers() as $value) {
            if ($user->compare($value)) {
                $this->loggedIn = true;
                return $this->isLoggedIn();
            }
        }
        return false;
    }

}
